import unittest

from app.geocode import geocode_handler

class TestGeocode(unittest.TestCase):

    def setUp(self):
        # set up mock querString for tests
        self.event = {
            'queryStringParameters':{
                'city': 'Portsmouth',
                'state': 'NH'
            }
        }
        self.context = {
            'function_name': 'ian-get-coords'
        }
    # test connection to Google API. Pass in dummy event and context to lambda code
    def test_api_connect(self):
        
        status_code = geocode_handler(self.event, self.context).get('statusCode')

        self.assertEqual(200, status_code)
    
    def test_bad_api_call(self):
        # test that 400 is return when query is missing state parm
        event = {
            'queryStringParameters':{
                'city': 'Portsmouth'
            }
        }

        status_code = geocode_handler(event,self.context).get('statusCode')

        self.assertEqual(400, status_code)


# if __name__ == '__main__':
#     unittest.main()