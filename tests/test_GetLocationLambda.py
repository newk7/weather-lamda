import unittest, json

from app.getLocationLambda import location_handler, get_location

class TestLocationLambda(unittest.TestCase):

    def setUp(self):
        # set up mock querString for tests
        self.event = {
            'queryStringParameters':{
                'ip': '18.204.136.254'
            }
        }
        self.context = {
            'function_name': 'ian-get-coords'
        }
    # test connection to Google API. Pass in dummy event and context to lambda code
    def test_location_api_connect(self):
        
        status_code = location_handler(self.event, self.context).get('statusCode')

        self.assertEqual(200, status_code)
    
    def test_no_ip_location(self):
        event = {
            'queryStringParameters':{
            }
        }
        status_code = location_handler(event, self.context).get('statusCode')

        self.assertEqual(400, status_code)
    
    def test_ip_viligante_error(self):

        status_code = get_location('foobar').get('statusCode')

        self.assertEqual(400, status_code)
    
    def test_location_return(self):

        coords = location_handler(self.event, self.context).get('body')
        # print(coords)
        coords = json.loads(coords)

        sample_dict = {
            'latitude': '42.36460',
            'longitude': '-71.10280'
        }
        self.assertDictEqual(sample_dict, coords)