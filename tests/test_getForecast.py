import unittest

from app.getWeather import lambda_handler

class TestLocationLambda(unittest.TestCase):

    def setUp(self):
        # set up mock querString for tests
        self.event = {
            'queryStringParameters':{
                "latitude": "42.3646",
                "longitude": "-71.10280"
            }
        }
        self.context = {
            'function_name': 'ian-get-coords'
        }
    # test connection to Google API. Pass in dummy event and context to lambda code
    def test_forecast_api_connect(self):
        
        status_code = lambda_handler(self.event, self.context).get('statusCode')

        self.assertEqual(200, status_code)
    
    def test_no_lat_location(self):
        event = {
            'queryStringParameters':{
                "longitude": "-71.10280"
            }
        }
        status_code = lambda_handler(event, self.context).get('statusCode')

        self.assertEqual(400, status_code)
    
    def test_no_long_location(self):
        event = {
            'queryStringParameters':{
                "longitude": "-71.10280"
            }
        }

        status_code = lambda_handler(event, self.context).get('statusCode')

        self.assertEqual(400, status_code)