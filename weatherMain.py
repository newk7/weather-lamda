import requests, json, argparse

parser = argparse.ArgumentParser(description='Get City,State from user')
parser.add_argument('--loc',action="store", dest="city,State", help='City,State to retrieve forecast for')
location = parser.parse_args()

def get_coords(city, state):
    url = "https://spuxyrmaf3.execute-api.us-east-2.amazonaws.com/default/ian-get-coords?city="+city+"&state="+state
    
    r = requests.get(url)
    r = r.json()

    latitude = str(r['latitude'])
    longitude = str(r['longitude'])

    return latitude, longitude


def get_pc_ip():
    r = requests.get('https://ipvigilante.com')
    ip = r.json()
    
    user_ip = ip['data']['ipv4']
    return user_ip

def get_location(ip_addr):
    r = requests.get('https://spuxyrmaf3.execute-api.us-east-2.amazonaws.com/default/get-location?ip=', ip_addr)
    
    r = r.json()
    lat = r['latitude']
    long = r['longitude']

    return lat, long

def get_forecast(latitude, longitude):
    r = requests.get('https://spuxyrmaf3.execute-api.us-east-2.amazonaws.com/default/get-forecast?latitude=' + latitude + '&longitude=' + longitude)
    forecast = json.loads(r.text)
    
    # forecast_dict = {
    #     "temp" : forecast['currently']['temperature'],
    #     "daily_summary" : forecast['daily']['data'][0]['summary'],
    #     "feels_like" :forecast['currently']['apparentTemperature'],
    #     "high" : forecast['daily']['data'][0]['temperatureMax'],
    #     "low" : forecast['daily']['data'][0]['temperatureMin'],
    #     "humid" : (forecast['currently']['humidity']) * 100
    # }
    # print(temp)
    return forecast

def print_forecast(forecast):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    # print("\nToday's forecast in",location)
    print("\n",forecast['daily_summary'])
    print("\nTemp:",forecast['temp'])
    print("Feels Like:",forecast["feels_like"])
    print("High:",forecast["high"])
    print("Low:",forecast["low"])
    print("Humidity:",forecast["humid"], "%")

if __name__ == "__main__":
    if location.cityState is None:
        #if user doesn't give location, get location from IP addr
        ip_addr = get_pc_ip()
        latitude, longitude= get_location(ip_addr)
        forecast = get_forecast(latitude, longitude)
        print_forecast(forecast)
    else:
        #user did provide location
        city, state = location.cityState.split(',')
        print('\n',city, state)
        latitude, longitude = get_coords(city, state)
        forecast = get_forecast(latitude, longitude)
        print_forecast(forecast)

