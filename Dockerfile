FROM python:3.7-alpine
RUN pip3 install requests
COPY weather.py /app/weather.py
ENTRYPOINT [ "python3", "/app/weather.py" ]