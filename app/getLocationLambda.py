#! /usr/bin/env python3
import requests, argparse, json, traceback, logging
from datetime import datetime

log = logging.getLogger()
log.setLevel(logging.INFO)

def location_handler(event, context):
    try:

        queryString = event['queryStringParameters']

        if not 'ip' in queryString:
            return {
                "statusCode": 400,
                "body": "Must IP address in query string"
            }
    
        ip = queryString['ip']
        longitude, latitude = get_location(ip)
        
        res = {'latitude': latitude, 'longitude': longitude}
        
        return{
            'statusCode': 200,
            'body': json.dumps(res)
        }
    except:
        log.error(traceback.format_exc())
        return {
            'statusCode': 500,
            'body': 'Error executing Lambda'
        }
    
def get_location(ip):
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    """

    r = requests.get('https://ipvigilante.com/'+ip)

    if r.status_code == 400:
        return {
            'statusCode': 400,
            'body': 'Invalid request to ipvigilante: %s' % r.text
        }
    elif r.status_code == 500:
        return {
            'statusCode': 500,
            'body': 'Error from IP Vigilante: %s' % r.text
        }
    location = r.json()
    # print(location['data']['latitude'])
    
    return location['data']['longitude'],location['data']['latitude']