#! /usr/bin/env python3
import requests, argparse
from datetime import datetime

DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"

parser = argparse.ArgumentParser(description='Process Time Stamp From User')
parser.add_argument('--time',action="store", dest="time_stamp")
time = parser.parse_args()


# print(time.time_stamp)

if time.time_stamp is not None:
    readable_date = str(datetime.fromtimestamp(int(time.time_stamp)))[:10]
    print(readable_date)


def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    """

    r = requests.get('https://ipvigilante.com')
    location = r.json()
    # print(location['data']['latitude'])
    
    return location['data']['longitude'],location['data']['latitude'],location['data']['city_name']

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    try:
    
        if time.time_stamp is None:
            r = requests.get('https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY+ '/' + latitude + ',' + longitude)
            forecast = r.json()
            forecast_dict = {
                "temp" : forecast['currently']['temperature'],
                "daily_summary" : forecast['daily']['data'][0]['summary'],
                "feels_like" :forecast['currently']['apparentTemperature'],
                "high" : forecast['daily']['data'][0]['temperatureMax'],
                "low" : forecast['daily']['data'][0]['temperatureMin'],
                "humid" : (forecast['currently']['humidity']) * 100
            }
            # print(temp)
            return forecast_dict
        else:
            r = requests.get('https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY+ '/' + latitude + ',' + longitude + ',' + time.time_stamp)
            forecast = r.json()
            high = forecast['daily']['data'][0]['temperatureMax']
            low = forecast['daily']['data'][0]['temperatureMin']

            return high, low
    except Exception:
        if time.time_stamp is None:
            return "err", "there was an error getting data", "err", "err", "err", "err"
        else:
            return "err", "err"
def print_forecast_current(location,forecast):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("\nToday's forecast in",location)
    print("\n",forecast['daily_summary'])
    print("\nTemp:",forecast['temp'])
    print("Feels Like:",forecast["feels_like"])
    print("High:",forecast["high"])
    print("Low:",forecast["low"])
    print("Humidity:",forecast["humid"], "%")

def print_forecast(location,high,low, time):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("\n The weather in ",location," on " + time + "\n High:",high,"\n Low:",low)

if __name__ == "__main__":
    longitude, latitude, town = get_location()
    if time.time_stamp is None:
        forecast = get_temperature(longitude, latitude)
        print_forecast_current(town, forecast)
    else:
        high, low = get_temperature(longitude, latitude)
        print_forecast(town, high, low, readable_date)

    