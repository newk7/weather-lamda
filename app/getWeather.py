#! /usr/bin/env python3
import json, requests
from datetime import datetime

DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"

def lambda_handler(event, context):
    try:
        queryString = event['queryStringParameters']

        if not ('latitude' in queryString and 'longitude' in queryString):
            return {
                'statusCode': 400,
                'body': 'Must include Lat and Long in query'
            }

        event_lat = queryString['latitude']
        event_long = queryString['longitude']

        # Query DarkSky API for forecast
        r = requests.get('https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY+ '/' + event_lat + ',' + event_long)
        
        if r.status_code == 400:
            return {
                'statusCode': 400,
                'body': 'Invalid request to DarkSky API'
            }
        elif r.status_code == 500:
            return {
                'statusCode': 500,
                'body': 'error from DarkSky API'
            }
        
        forecast = r.json()
        forecast_dict = {
            "temp" : forecast['currently']['temperature'],
            "daily_summary" : forecast['daily']['data'][0]['summary'],
            "feels_like" :forecast['currently']['apparentTemperature'],
            "high" : forecast['daily']['data'][0]['temperatureMax'],
            "low" : forecast['daily']['data'][0]['temperatureMin'],
            "humid" : (forecast['currently']['humidity']) * 100
        }
        return {
            'statusCode': 200,
            'body': json.dumps(forecast_dict)
        }

    except:
        return {
            'statusCode': 500,
            'body': 'error executing get_weather lambda'
        }

# def get_temperature(longitude, latitude):
    
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    
    
    # r = requests.get('https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY+ '/' + latitude + ',' + longitude)
    # forecast = r.json()
    # forecast_dict = {
    #     "temp" : forecast['currently']['temperature'],
    #     "daily_summary" : forecast['daily']['data'][0]['summary'],
    #     "feels_like" :forecast['currently']['apparentTemperature'],
    #     "high" : forecast['daily']['data'][0]['temperatureMax'],
    #     "low" : forecast['daily']['data'][0]['temperatureMin'],
    #     "humid" : (forecast['currently']['humidity']) * 100
    # }
    # # print(temp)
    # return forecast_dict